#!/bin/bash

case ${1} in
     outlet)
        outletPower=`snmpwalk 192.160.15.23 -v1 -c public .1.3.6.1.4.1.17420.1.2.9.$2.11.0 | grep -oP "(?<=INTEGER: )[^ ]+"`
        echo "Outlet power $outletPower | Outlet power=$outletPower;;"
        exit 0
     ;;
     overall)
        overallPower=`snmpwalk 192.160.15.23 -v1 -c public .1.3.6.1.4.1.17420.1.3.7.0 | grep -oP "(?<=STRING: )[^ ]+" | tr -d '"'`
        echo "Overall power=$overallPower | Overall power=$overallPower;;"
        exit 0
     ;;
esac
exit 0
