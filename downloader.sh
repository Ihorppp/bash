#!/bin/bash

file_with_links="$1"
folder="$2"
u="here_goes_url"

while read -r line
do
	url=$u$line
	image="curl -s $url"
	if [ `$image | cat | grep AccessDenied | wc -w` -eq 0 ]
		then
			`$image > $folder$line`
	fi
done < $file_with_links