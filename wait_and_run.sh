#!/bin/bash

pointer="/tmp/pointer_to_run"
time_limit=14400
sleep_time=10
time_counter=0

while :
do
	if [ -f $pointer ]; then
		break
	fi;

	sleep $sleep_time

	let time_counter+=sleep_time

	if [ $time_counter -ge $time_limit ]; then
		exit 0
	fi;

done;

n_lines=`cat $pointer | wc -l`

let start=$1%$n_lines

#first part
for (( i=$start; i<=$n_lines; i++ )); do
	`head -$i $pointer | tail -1 | cut -d '"' -f2`
done;

#second part
for (( i=1; i<$start; i++ )); do
	`head -$i $pointer | tail -1 | cut -d '"' -f2`
done;