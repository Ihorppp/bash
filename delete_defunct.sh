#!/bin/bash

`ps ax | grep defunc > defuncs.txt`

while read line 
     do
         pid="$(echo $line | cut -d ' ' -f1)"
         ppid="$(ps -f $pid | cut -d ' ' -f3)"
         kill -9 $ppid 
     done < defuncs.txt


